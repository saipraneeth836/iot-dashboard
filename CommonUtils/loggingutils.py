import logging
from logging.handlers import TimedRotatingFileHandler

def get_module_logger(mod_name):
    """
    This function is the core logger module. All the logging_dir related configurations are defined here
    :param mod_name:
    :return:
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
          '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    #if mod_name == "longResearch":
    fhandler = TimedRotatingFileHandler('logs/glens-aermod.log', when="d", interval=1, backupCount=5)

    # if mod_name == "bm_chatUpload":
    #     fhandler = TimedRotatingFileHandler('batchmodule_chatuploads3.log', when="d", interval=1, backupCount=5)
    fhandler.setLevel(logging.INFO)
    # create a logging_dir format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fhandler.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fhandler)
    return logger