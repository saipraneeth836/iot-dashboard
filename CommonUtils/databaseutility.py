import pymysql
# from ConfigParser import SafeConfigParser
from Configs.configurations import Config

# from scripts.logging_dir.logging_file import logger
from CommonUtils.loggingutils import get_module_logger
logger = get_module_logger("longResearch")

class MySqlClient(object):

    def __init__(self):
        self.host = None
        self.username = None
        self.password = None
        self.db_name = None
        self.db = None
        self.cur = None
        self.read_configurations()

    def read_configurations(self):

        try:
            self.host = Config.DB_HOST
            self.username = Config.DB_USER_NAME
            self.password = Config.DB_PASSWORD
            self.db_name = Config.DB_NAME
        except Exception as e:
            logger.error("Exception while Fetching Config Details: "+str(e))

    def connect(self):
        try:
            self.db = pymysql.connect(self.host, self.username, self.password, self.db_name,charset='utf8',
            use_unicode=True)
            #self.db = MySQLdb.connect(self.host, self.username, self.password, self.db_name)
            self.db.autocommit(True)
            self.cur = self.db.cursor()
            return self.cur
        except Exception as e:
            logger.error(str(e))

    # handler to be called to on every query insert/update/fetch everything
    def execute_query(self, query, params):
        try:
            for key, value in params.items():
                value = str(value).replace("'", "''")
                query = query.replace(key, str(value))
        except:
            pass

        try:
            self.connect()
            self.cur.execute(query)
            cursor = self.cur  # Connecting here only
            result = {"data":[] , "inserted":cursor.lastrowid}
            result['data'] = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
            return result

        except Exception as e:
            logger.error("Exception while Fetching: "+str(e))
            return False


    def execute_fetchone(self, query):
        try:
            cursor = self.connect()
            cursor.execute(query)
            result = cursor.fetchone()
            cursor.close()
            return result
        except Exception as e:
            logger.error("Exception while Fetching: "+str(e))
            return False

    def update_table(self, query,data=[]):
        try:
            cursor = self.connect()
            cursor.execute(query,data)
            result = cursor.fetchone()
            cursor.close()
            return True,result
        except Exception as e:
            logger.error("Exception while updating:  :"+str(e))
            return False


    def close_connection(self):
        self.db.close()