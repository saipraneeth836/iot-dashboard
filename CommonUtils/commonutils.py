import base64
from base64 import b64decode
import json
from datetime import datetime
from CommonUtils.databaseutility import MySqlClient
from Configs import app_constants
from CommonUtils.encryptionutils import EncryptionUtils
from flask import make_response

encryption_obj = EncryptionUtils()
db_obj = MySqlClient()


def parse_input_request(request_data):
    content = (request_data.get_data().decode())
    try:
        content = json.loads(b64decode(content))
    except Exception as e:
        # logger.error("Error decoding request json {}".format(str(e)))
        print('e is ',e)

    return content


def parse_output_response(response_data):
    base_64_response = dict()
    try:
        encoded_dict = json.dumps(response_data).encode('utf-8')
        base_64_response = base64.b64encode(encoded_dict)
    except Exception as e:
        # logger.error("Error encoding JSON {}".format(str(e)))
        print('e is ', e)
    return base_64_response


def return_with_header(output_json, header_name=None, header_value=None):
    response = make_response()
    if header_value is not None:
        response.headers['x-frame-options'] = 'DENY'
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers[header_name] = header_value
    elif header_name is not None:
        response.headers['x-frame-options'] = 'DENY'
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers["Access-Control-Allow-Headers"] = "x-requested-with"
        response.headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
        response.headers["Access-Control-Allow-Headers"] = "Origin, Methods, Content-Type"
    else:
        response.headers['x-frame-options'] = 'DENY'
        response.headers['Access-Control-Allow-Origin'] = '*'
    response.set_data(output_json)
    return response


def audit_log(user_name, audit_value, user_role):
    current_time = datetime.now()
    try:
        audit_query = '''INSERT INTO tbl_audit(fld_username,fld_action,TimeStamp fld_userRole)
                      values ('{0}','{1}','{2}','{3}')'''.format(user_name, audit_value, str(current_time), user_role)
        try:
            db_obj.execute_fetchone(audit_query)
        except Exception as e:
            print('e is ', e)
            # logger.error("Failed query {}".format(audit_query))
            # logger.error("Error occurred in audit_log query {}".format(str(e)))
    except Exception as e:
        print('e is ', e)
        # logger.exception("Exception in audit log {}".format(str(e)))
    @staticmethod
    def read_license_file():
        """
        function to read the license file.
        :return:
        """
        file_obj = None
        license_json = {"status": "failed"}
        try:
            file_obj = open(app_constants.license_file, 'r')
            license_json = json.load(file_obj)
            license_json["status"] = "success"
            license_json["message"] = "success"
            file_obj.close()
        except Exception as e:
            file_obj.close()
            # logger.exception("Exception occurred when reading the LicenseFile.json: {}".format(str(e)))
            license_json["message"] = "Error fetching LicenseFile.json."
        return license_json

    def validate_license(self):
        """
        Function to return the validity status of license.
        :return:
        """
        try:
            license_json = self.read_license_file()
            if license_json["status"] == "failed":
                # logger.error("Error Reading License File")
                return False
            else:
                return self.verify_signature(license_json["licenseKey"])
        except Exception as e:
            print('e is ',e)
            # logger.error("Error Validating license {}".format(str(e)))
