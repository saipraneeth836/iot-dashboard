from __future__ import division, absolute_import, print_function

import binascii
import os
import struct
import sys
import random
import math
from collections import namedtuple
from binascii import hexlify, unhexlify
from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

class EncryptionUtils(object):
    """
    Utility Class for data Encryption.
    """

    def __init__(self):
        py3 = sys.version_info[0] == 3
        if py3:
            self.binary_type = bytes
            self.range_func = range
        else:
            self.binary_type = str
            self.range_func = range
        self.key_pair = namedtuple('KeyPair', 'public private')
        self.key = namedtuple('Key', 'exponent modulus')

    @staticmethod
    def encrypt_file(key, in_filename, out_filename="", chunk_size=64 * 1024):
        """
        Used for Encrypting Site.conf
        :param key:
        :param in_filename:
        :param out_filename:
        :param chunk_size:
        :return:
        """
        if not out_filename:
            out_filename = in_filename + '.enc'
        iv = get_random_bytes(16)
        encryption_cipher = AES.new(key.encode("utf8"), AES.MODE_CBC, iv)
        file_size = os.path.getsize(in_filename)

        with open(in_filename, 'rb') as infile:
            with open(out_filename, 'wb') as outfile:
                outfile.write(struct.pack('<Q', file_size))
                outfile.write(iv)
                while True:
                    chunk = infile.read(chunk_size)
                    if len(chunk) == 0:
                        break
                    elif len(chunk) % 16 != 0:
                        chunk += b' ' * (16 - len(chunk) % 16)
                    outfile.write(encryption_cipher.encrypt(chunk))

    @staticmethod
    def decrypt_file(key, in_filename, out_filename="", chunk_size=24 * 1024):
        """
        Used for Decrypting Site.conf
        :param key:
        :param in_filename:
        :param out_filename:
        :param chunk_size:
        :return:
        """
        if not out_filename:
            out_filename = os.path.splitext(in_filename)[0]

        with open(in_filename, 'rb') as infile:
            original_size = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
            iv = infile.read(16)
            decryption_cipher = AES.new(key.encode("utf8"), AES.MODE_CBC, iv)
            with open(out_filename, 'wb') as outfile:
                while True:
                    chunk = infile.read(chunk_size)
                    if len(chunk) == 0:
                        break
                    outfile.write(decryption_cipher.decrypt(chunk))
                outfile.truncate(original_size)

    @staticmethod
    def site_conf_decryption(key, in_filename, chunk_size=24 * 1024):
        """
        This function is to decrypt the file
        :param key:
        :param in_filename :
        :param chunk_size :
        :return decrypt_temp:
        """
        decrypt_temp = ""
        with open(in_filename, 'rb') as infile:
            struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
            iv = infile.read(16)
            decrypt = AES.new(key, AES.MODE_CBC, iv)
            while True:
                chunk = infile.read(chunk_size)
                if len(chunk) == 0:
                    break
                temp = decrypt.decrypt(chunk)
                decrypt_temp = decrypt_temp + str(temp)
        return decrypt_temp

    # Rabin Primality Test
    def is_prime(self, n, k=30):
        # http://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
        if n <= 3:
            return n == 2 or n == 3
        neg_one = n - 1
        # write n-1 as 2^s*d where d is odd
        s, d = 0, neg_one
        while not d & 1:
            s, d = s + 1, d >> 1
        assert 2 ** s * d == neg_one and d & 1
        for _ in self.range_func(k):
            a = random.randrange(2, neg_one)
            x = pow(a, d, n)
            if x in (1, neg_one):
                continue
            for _ in self.range_func(s - 1):
                x = x ** 2 % n
                if x == 1:
                    return False
                if x == neg_one:
                    break
            else:
                return False
        return True

    def random_prime(self, n=10 ** 8):
        p = 1
        while not self.is_prime(p):
            p = random.randint(n)
        return p

    @staticmethod
    def multiplicative_inverse(modulus, value):
        """
        Multiplicative inverse in a given modulus
        multiplicative_inverse(191, 138)
        18
        multiplicative_inverse(191, 38)
        186
        multiplicative_inverse(120, 23)
        47
        """
        # http://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
        x, last_x = 0, 1
        a, b = modulus, value
        while b:
            a, q, b = b, a // b, a % b
            x, last_x = last_x - q * x, x
        result = (1 - last_x * modulus) // value
        if result < 0:
            result += modulus
        assert 0 <= result < modulus and value * result % modulus == 1
        return result

    def keygen(self, n, public=None):
        """
        Generate public and private keys from primes up to N.
        Optionally, specify the public key exponent (65537 is popular choice).
        pubkey, privkey = keygen(2**64)
        msg = 123456789012345
        coded = pow(msg, *pubkey)
        plain = pow(coded, *privkey)
        assert msg == plain
        """
        # http://en.wikipedia.org/wiki/RSA
        prime1 = self.random_prime(n)
        prime2 = self.random_prime(n)
        composite = prime1 * prime2
        totient = (prime1 - 1) * (prime2 - 1)
        if public is None:
            while True:
                private = random.randrange(totient)
                if math.gcd(private, totient) == 1:
                    break
            public = self.multiplicative_inverse(totient, private)
        else:
            private = self.multiplicative_inverse(totient, public)
        assert public * private % totient == math.gcd(public, totient) == math.gcd(private, totient) == 1
        assert pow(pow(1234567, public, composite), private, composite) == 1234567
        return self.key_pair(self.key(public, composite), self.key(private, composite))

    # Method for encrypting the data
    def encrypt(self, msg, public_key, verbose=False):
        chunk_size = int(math.log(public_key.modulus, 256))
        output_chunk = chunk_size + 1
        output_format = '%%0%dx' % (output_chunk * 2,)
        binary_msg = msg if isinstance(msg, self.binary_type) else msg.encode('utf-8')
        result = list()
        for start in self.range_func(0, len(binary_msg), chunk_size):
            chunk = binary_msg[start:start + chunk_size]
            chunk += b'\x00' * (chunk_size - len(chunk))
            plain = int(hexlify(chunk), 16)
            coded = pow(plain, *public_key)
            binary_encoded = unhexlify((output_format % coded).encode())
            if verbose:
                print('Encode:', chunk_size, chunk, plain, coded, binary_encoded)
            result.append(binary_encoded)
        return binascii.b2a_base64(b''.join(result))

    # Method for decrypting the data
    def decrypt(self, msg, private_key, verbose=False):
        binary_cipher = binascii.a2b_base64(msg)
        chunk_size = int(math.log(private_key.modulus, 256))
        output_chunk = chunk_size + 1
        output_format = '%%0%dx' % (chunk_size * 2,)
        result = []
        for start in self.range_func(0, len(binary_cipher), output_chunk):
            binary_encoded = binary_cipher[start: start + output_chunk]
            coded = int(hexlify(binary_encoded), 16)
            plain = pow(coded, *private_key)
            chunk = unhexlify((output_format % plain).encode())
            if verbose:
                print('Decode:', chunk_size, chunk, plain, coded, binary_encoded)
            result.append(chunk)
        return b''.join(result).rstrip(b'\x00').decode('utf-8')

    # Converting the key to string
    @staticmethod
    def key_to_str(key):
        """
        Convert `Key` to string representation
        key_to_str(Key(50476910741469568741791652650587163073, 95419691922573224706255222482923256353))
        '25f97fd801214cdc163796f8a43289c1:47c92a08bc374e96c7af66eb141d7a21'
        """
        return ':'.join((('%%0%dx' % ((int(math.log(number, 256)) + 1) * 2)) % number) for number in key)

    # Converting a  string to key
    def str_to_key(self, key_str):
        """
        Convert string representation to `Key` (assuming valid input)
        (str_to_key('25f97fd801214cdc163796f8a43289c1:47c92a08bc374e96c7af66eb141d7a21') ==
        Key(exponent=50476910741469568741791652650587163073, modulus=95419691922573224706255222482923256353))
        True
        """
        return self.key(*(int(number, 16) for number in key_str.split(':')))
