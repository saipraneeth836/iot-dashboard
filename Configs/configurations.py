import os


class Config(object):
    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
    FLASK_APP = 'metadata_portal.py'
    FLASK_ENV = 'development'
    FLASK_APP_PORT = 5000
    FILE_URL = 'http://192.168.1.102:5000/'
    FLASK_APP_HOST = '192.168.1.102'
    FLASK_DEBUG = 1
    SECRET_KEY = 'fdhfjkjhfdsfiuhfifhoud3247348938@i3943789ddh'
    MAIL_SERVER = 'localhost'
    MAIL_SERVER_TYPE = 'local'
    MAIL_SENDER = 'IOT'
    MAIL_RECEIVERS = ['nitesh.agarwal@knowledgelens.com']
    MAIL_DOMAIN_TYPE = 'http://localhost:4200/#/'
    DEPLOYED_PORT = 5432
    DB_HOST = 'localhost'
    DB_NAME = 'iot_dilogy'
    DB_USER_NAME = 'root'
    DB_PASSWORD = 'root'
    DB_PORT = '3306'