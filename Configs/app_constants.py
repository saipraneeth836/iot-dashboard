import os
from configparser import ConfigParser

from CommonUtils.encryptionutils import EncryptionUtils

encryption_obj = EncryptionUtils()

current_version = "v1.0"
encryption_key = "0123456701234567"
license_validation_key = "165158f1a5750d4c7c68585d94788f1f:1c8cdfe11210a0dd5996f9298d0ce2af"
license_file = os.getcwd() + r"\\assets\\conf\\LicenseFile.json"
decryption_site_conf = os.getcwd() + r"\\assets\\conf\\DecryptionSite.conf"
log_delete_frequency = 10
host = "192.168.1.87"
username = "root"
password = "root"
db_name = "iot_dilogy"